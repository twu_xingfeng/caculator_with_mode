import * as React from "react";
import './ValueInput.less'


export default class ValueInput extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <input onChange={this.props.handler} className={this.props.class} value={this.props.value}/>
        );
    }
}