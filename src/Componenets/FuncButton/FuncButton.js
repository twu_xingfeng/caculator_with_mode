import * as React from "react";
import './FuncButton.less'


export default class FuncButton extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <button onClick={this.props.click} className={this.props.class}>{this.props.name}</button>
        );
    }
}