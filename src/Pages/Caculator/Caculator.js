import FuncButton from "../../Componenets/FuncButton/FuncButton";
import * as React from "react";
import './Caculator.less'
import ValueInput from "../../Componenets/ValueInput/ValueInput";
export default class Calculator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            val1:0,
            val2:0,
            result:0,
            mode:0
        }
    }

    val1Change(e){
        this.setState({
            val1:e.currentTarget.value
        });
    }

    val2Change(e){
        this.setState({
            val2:e.currentTarget.value
        });
    }

    funcClick(mode){
        if(mode===6){
            let result = 0;
            switch (this.state.mode) {
                case 1: //%
                    let val2 = this.state.result % this.state.val1;
                    this.setState({
                        val2:val2
                    });
                    break;
                case 2: //+
                    result = this.state.val2 + this.state.val1;
                    this.setState({
                        result:result
                    });
                    break;
                case 3: //x
                    result = this.state.val2 * this.state.val1;
                    this.setState({
                        result:result
                    });
                    break;
                case 4: //-
                    result = this.state.val1 - this.state.val2;
                    this.setState({
                        result:result
                    });
                    break;
                case 5: ///
                    result = this.state.val1 / this.state.val2;
                    this.setState({
                        result:result
                    });
                    break;
            }
        }else if(mode===0){
            this.setState({
                val1:0,
                val2:0,
                result:0,
                mode:0
            });
        }else{
            this.setState({
                mode:mode
            });
        }
    }

    render() {
        return (
            <section className="caculator-panel">
                <section className="cacualtor-func-area">
                <FuncButton click={this.funcClick.bind(this,0)} class="func-button" name="AC"/>
                <FuncButton click={this.funcClick.bind(this,1)} class="func-button" name="%"/>
                <FuncButton click={this.funcClick.bind(this,2)} class="func-button" name="+"/>
                <FuncButton click={this.funcClick.bind(this,3)} class="func-button" name="X"/>
                <FuncButton click={this.funcClick.bind(this,4)} class="func-button" name="-"/>
                <FuncButton click={this.funcClick.bind(this,5)} class="func-button" name="/"/>
                <FuncButton click={this.funcClick.bind(this,6)} class="equal-button" name="="/>
                </section>
                <section className="cacualtor-func-area">
                    <ValueInput class="result-input" value={this.state.result}/>
                    <ValueInput handler={this.val1Change.bind(this)} class="code-input" value={this.state.val1}/>
                    <ValueInput handler={this.val2Change.bind(this)} class="code-input" value={this.state.val2}/>
                </section>

            </section>
        );
    }
}